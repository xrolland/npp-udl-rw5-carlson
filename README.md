# Carlson RW5 syntax highlighting with Notepad++
## Description
Basic rules to render syntax highlighting with Notepad++ for Carlson RW5 files as a [User Defined Language (UDL)](https://ivan-radic.github.io/udl-documentation/). Theses files are created by Carlson SurvCE/SurvPC software to record land survey raw observations.

Currently, it's very very basic, but I hope to be able to improve it. Help appreciated!

## Specifications
 - Carlson RW5 file format: [SurvCE Raw Data File Format (*.rw5) –Version 3.03](http://web.carlsonsw.com/files/knowledgebase/kbase_attach/223/Raw%20File%20Records_3.03.pdf) 
 - TDS RW5 from which Carlson RW5 is based: [Tripod Data Systems, Inc. Raw Data Record Specification - Survey Pro™ Version 4.0](https://communities.bentley.com/cfs-filesystemfile/__key/telligent-evolution-components-attachments/00-5922-01-00-00-14-89-65/TDS-RAW-Data-Specifications.pdf)
 - SurvCE online manual: [Explanations about RW5 processing in SurvCE](http://files.carlsonsw.com/mirror/manuals/SurvCE/online/source/ProcessRawFile.html) 
 - [Carlson RW5 page on FLOSS TotalOpenStation](https://totalopenstation.readthedocs.io/en/stable/input_formats/if_carlson_rw5.html)